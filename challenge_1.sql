-- Number of transactions, approvals, and total declines for each day.

SELECT transaction_date, sum(total_return_code), transaction_type
FROM (SELECT 
	COUNT(t.return_code) total_return_code,
	CASE WHEN t.return_code = 0 THEN
		'Approved'
	ELSE
		'Declined'
	END transaction_type,
	t.date transaction_date
FROM transactions t
GROUP BY t.return_code, t.date)
GROUP BY transaction_date,transaction_type
ORDER BY 1;
