-- Display the number of approval codes for each merchant for each day.

SELECT merchant_id, SUM(approved_return_code) 
FROM (SELECT
	t.merchant merchant_id,
	COUNT(t.return_code) approved_return_code
FROM transactions t
where t.return_code = 0
GROUP BY t.merchant)
GROUP BY merchant_id;
