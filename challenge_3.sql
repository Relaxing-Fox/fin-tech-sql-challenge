-- Display the total profit for each merchant for each day.

with merchant_id as (
	select distinct t1.merchant
	from transactions t1
),
return_code_0 as (
	select t3.merchant merchant_id_0, t2.return_code return_code_0, COUNT(t2.return_code) return_code_0_count
	from transactions t2
	join merchant_id t3 on t3.merchant = t2.merchant 
	where t2.return_code = 0
	GROUP BY t3.merchant, t2.return_code
),
return_code_1 as (
	select t5.merchant_id_0 merchant_id_1, t4.return_code return_code_1, COUNT(t4.return_code) return_code_1_count
	from transactions t4
	join return_code_0 t5 on t5.merchant_id_0 = t4.merchant 
	where t4.return_code = 1
	group by t5.merchant_id_0, t4.return_code
),
return_code_21 as (
	select t7.merchant_id_1 merchant_id_21, t6.return_code return_code_21, COUNT(t6.return_code) return_code_21_count
	from transactions t6
	join return_code_1 t7 on t7.merchant_id_1 = t6.merchant 
	where t6.return_code = 21
	group by t7.merchant_id_1, t6.return_code
),
return_code_36 as (
	select t9.merchant_id_21 merchant_id_36, t8.return_code return_code_36, COUNT(t8.return_code) return_code_36_count
	from transactions t8
	join return_code_21 t9 on t9.merchant_id_21 = t8.merchant 
	where t8.return_code = 36
	group by t9.merchant_id_21, t8.return_code
),
return_code_40 as (
	select t11.merchant_id_36 merchant_id_40, t10.return_code return_code_40, COUNT(t10.return_code) return_code_40_count
	from transactions t10
	join return_code_36 t11 on t11.merchant_id_36 = t10.merchant 
	where t10.return_code = 40
	group by t11.merchant_id_36, t10.return_code
),
profit as (
	select
		m.merchant,
		(SUM(rcd0.return_code_0_count) * .10) profit_0,
		(SUM(rcd1.return_code_1_count) * .05) profit_1,
		(SUM(rcd21.return_code_21_count) * 0) profit_21,
		(SUM(rcd36.return_code_36_count) * .07) profit_36,
		(SUM(rcd40.return_code_40_count) * .02) profit_40
	from merchant_id m
	join return_code_0 rcd0 on rcd0.merchant_id_0 = m.merchant
	join return_code_1 rcd1 on rcd1.merchant_id_1 = m.merchant
	join return_code_21 rcd21 on rcd21.merchant_id_21 = m.merchant
	join return_code_36 rcd36 on rcd36.merchant_id_36 = m.merchant
	join return_code_40 rcd40 on rcd40.merchant_id_40 = m.merchant
	group by m.merchant
)
select
	p.merchant merchant_id, 
	'$' || (p.profit_0 - p.profit_1 - p.profit_21 - p.profit_36 - p.profit_40) total_profit
from profit p
order by 1;
