# Fin-Tech SQL Challenge

I am providing a CSV file of transactions that merchants have made from a Fin-Tech company over a certain period of time.

| date       | merchant| return_code  |
|------------|---------|----|
| 2019-01-01 | 3899978 | 1  |
| 2019-01-01 | 1005527 | 0  |
| 2019-01-01 | 3899978 | 0  |
| 2019-01-01 | 1005527 | 0  |
| 2019-01-01 | 1002349 | 1  |
| 2019-01-01 | 1005527 | 40 |
| 2019-01-01 | 1005527 | 0  |

Possible transaction return codes are as follows:
- 0,  transaction approved
- 1,  transaction declined, insufficient funds
- 21, transaction declined, card reported stolen
- 36, transaction declined, improper format
- 40, transaction declined, unknown reason

There is a profit margin associated with each return code:
- 0, 10 cents
- 1, -5 cents
- 21, 0 cents
- 36, -7 cents
- 40, -2 cents

I demostrated these problems below by generating a result set for each problem. 

- Display the number of transactions, approvals, and total declines for each day. (completed)
- Display the number of approval codes for each merchant for each day. (completed)
- Display the total profit for each merchant for each day. (completed)
